{-
    Haskell program for Functional Programming CA1
    Oshi Kavadia - d00169990
    -}
import qualified Data.CaseInsensitive as CI
import System.IO
import qualified Data.List as List


--Q1) function to add two numbers and double them
add_and_double::(Num a)=>a->a->a
add_and_double x y = (x+y)*2

--Q2)Making an infix operator '+*' That does the same thing as the function above
(+*)::(Num a)=>a->a->a
(+*) x y = x `add_and_double` y


{--Q3)solve_quadratic_equation function with arguements a b c
http://www.purplemath.com/modules/quadform.htm  -- reference site for the formula
Asked Minh Thu Le to help me verify the implementation of formula used
-}
solve_quadratic_equation::Double->Double->Double->(Double,Double)
solve_quadratic_equation a b c =  (x,y)
                          where 
                            x = b' + sqr/(2*a)
                            y = b' - sqr/(2*a)
                            b' = -b/(2*a)
                            sqr = (sqrt(b ** 2 - (4 * a * c)))



--Q4)Function that takes Int n and returns a list of number from 1 to n
first_n::Int->[Int]
first_n n 
         | n < 0 = error "Cannot take a negative value..."
         | otherwise = take n [1..]



--Q5)first_n_integer implements the above defined function using Integers and a local function called take_integer
first_n_integer :: Integer->[Integer]
first_n_integer n = take_integer n [1..]
        where take_integer :: Integer->[Integer]->[Integer]
              take_integer n (x:xs)
                                |n < 0 = error "Cannot take negative value..."
                                |n ==  0 = []
                                |otherwise = x : take_integer (n-1) xs



--Q6) This function returns the product of all factorials from 0 to n. (0!*1!*2!.....*n!)
--Help from lecturer John Loane to understand the question 
double_factorial::Integer->Integer
double_factorial n 
                |n <=0 = 1
                |otherwise = double_factorial (n-1) * factorial(n)
                  where factorial::Integer->Integer
                        factorial n 
                                |n <= 0 = 1
                                |otherwise = n*factorial(n-1)


--Q7) This function returns an infinite list of factorials using zipWith function
factorials::[Integer]
factorials = 1 : zipWith (*) [1..] factorials


--Q8)This function checks if a function is prime and returns a boolean value
isPrime::Integer->Bool
isPrime n 
        |n <= 1 = False
        |otherwise = not $ elem 0 ( map (mod n) [2..k] )
            where k = round $ sqrt(fromIntegral n)

--Q9)Returns an infinte list of primes
primeList::[Integer]
primeList = filter isPrime [1..]

--Q10) Summing a list of numbers recusively 
sumRecursion :: [Integer]->Integer
sumRecursion [] = 0
sumRecursion (x:xs) = x + sumRecursion xs


--Q11) Summing a list using foldl
sumFoldl::(Num a)=>[a]->a
sumFoldl = foldl(\acc x -> acc + x)0


--Q12) Multiply a list of integer using foldr
multiplyFoldr::(Num a)=>[a]->a
multiplyFoldr = foldr(*)1


{-Q13)A function that takes in a string and an int and return a boolean value depending on the condition specified
Case for '5' not specified in the specifications so assumed to be the loosing case
https://www.reddit.com/r/haskell/comments/28ivpz/newb_question_why_isnt_my_guess_number_game/
http://stackoverflow.com/questions/13190314/haskell-do-monad-io-happens-out-of-order
https://wiki.haskell.org/Introduction_to_Haskell_IO/Actions
-}
guess::String->Int->IO()
guess s n = 
 if CI.foldCase s == CI.foldCase "i love functional programming" && n < 5 then putStrLn "You win!" 
 else if CI.foldCase s == CI.foldCase "i love functional programming" || CI.foldCase s /= CI.foldCase "i love functional programming"  && n >= 5 
  then putStrLn "You loose!" 
 else do
        putStrLn "Oops, guess again\n"
        putStrLn "Enter Your guess String..."
        
        line <- getLine
        putStrLn "Enter your guess Number..."
        
        numberString <- getLine
        let number = read numberString :: Int
        guess (read line) (number)



--Q14)Finds the dot product of two vectors of any size
dot_product::(Num a)=>[a]->[a]->a
dot_product xs ys = sum $ zipWith (*) xs ys


--Q15)A function that checks is a number is even i.e n mod 2 == 0
is_even::Integer->Bool
is_even n = n `mod` 2 == 0 


--Q16)Function to get a unix name , i.e a string with no vowels
unixname :: String->String
unixname [] = error "Empty String..."
unixname cs =[c | c<- cs,not $  c `elem` "AEIOUaeiou"]


--Q17)This function finds the intersection from two lists.
intersection::(Eq a)=>[a]->[a]->[a]
intersection = List.intersect


--Q18)This function censors or replaces each vowel with x in a string
censor::String->String
censor = map(\c -> if c `elem` "AEIOUaeiou" then 'x' else c)

